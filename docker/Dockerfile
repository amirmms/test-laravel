FROM php:8.2-apache

ARG USER=docker
ARG UID=1000
ARG GID=1000
ARG PW=docker
RUN useradd -m ${USER} --uid=${UID} && echo "${USER}:${PW}" | chpasswd && adduser ${USER} sudo
USER root

RUN apt-get update && apt-get install -y \
    curl \
    g++ \
    git \
    libbz2-dev \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libonig-dev \
    libpng-dev \
    libreadline-dev \
    libxml2-dev \
    libzip-dev \
    sudo \
    unzip \
    zip
RUN rm -rf /var/lib/apt/lists/*

# Apache
USER ${USER}
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout /home/${USER}/server.key -out /home/${USER}/server.crt \
    -subj "/C=../ST=../L=../O=../CN=.."
USER root
ENV APACHE_RUN_USER=${USER}
RUN echo ${APACHE_RUN_USER}
ENV APACHE_RUN_GROUP=${USER}
ENV APACHE_LOG_DIR=/var/log/apache2$SUFFIX
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
ENV APACHE_PID_FILE=/var/run/apache2/apache2$SUFFIX.pid
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN sed -ri -e "s!/etc/ssl/certs/ssl-cert-snakeoil.pem!/home/${USER}/server.crt!g" /etc/apache2/sites-available/default-ssl.conf
RUN sed -ri -e "s!/etc/ssl/private/ssl-cert-snakeoil.key!/home/${USER}/server.key!g" /etc/apache2/sites-available/default-ssl.conf
RUN a2enmod rewrite headers ssl
RUN a2ensite default-ssl.conf

# PHP
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
COPY php.ini "$PHP_INI_DIR/conf.d/99-custom.ini"
RUN pecl install redis && docker-php-ext-enable redis
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install \
    -j$(nproc) gd \
    bcmath \
    bz2 \
    calendar \
    iconv \
    intl \
    mbstring \
    opcache \
    pcntl \
    pdo_mysql \
    soap \
    zip

# Compoer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

USER ${USER}

# Node
ENV NODE_VERSION=16.13.0
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
ENV NVM_DIR=/home/${USER}/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="${NVM_DIR}/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

WORKDIR /var/www/html

EXPOSE 80 443
