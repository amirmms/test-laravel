<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $rules = [
            'password' => 'required|string|min:6|max:32',
            'username' => 'required',
        ];
        $this->validate($request, $rules);

        $client = new Client();
        $data = [
            'username' => $request->username,
            'password' => $request->password,
            'grant_type' => 'password',
            'scope' => '*',
            'provider' => 'users',
            'client_id' => config('app.passport.users.client_id'),
            'client_secret' => config('app.passport.users.client_secret'),
        ];

        $response = $client->request(
            'POST',
            config('app.url') . '/oauth/token',
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'json' => $data,
                'http_errors' => false,
                'verify' => false,
            ]
        );

        return response()->json(
            json_decode($response->getBody(), true),
            $response->getStatusCode()
        );
    }

    public function profile()
    {
        $user = auth()->user();

        $permissions = Cache::remember(
            'api-user-profile-info-' . $user->id,
            (app('env') == 'production') ? 3600 : 0,
            function () use ($user) {
                return $user
                    ->getAllPermissions()
                    ->filter(
                        function ($item) {
                            return Str::startsWith($item->name, 'api.');
                        }
                    )
                    ->pluck('name')
                    ->toArray();
            }
        );

        $roles = Cache::remember(
            'api-user-profile-roles-info-' . $user->id,
            (app('env') == 'production') ? 3600 : 0,
            function () use ($user) {
                return $user->roles->pluck('name')->toArray();
            }
        );

        return [
            'user' => new UserResource($user),
            'roles' => $roles,
            'permissions' => $permissions,
        ];
    }

    // public function register(Request $request)
    // {
    //     $request->validate([
    //         'name' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:255|unique:users',
    //         'password' => 'required|string|min:6',
    //     ]);

    //     $user = User::create([
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //     ]);

    //     return response()->json([
    //         'message' => 'User created successfully',
    //         'user' => $user
    //     ]);
    // }

    public function logout()
    {
        auth()->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }
}
