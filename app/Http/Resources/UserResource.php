<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'mobile' => $this->mobile,
            'national_id' => $this->national_id,

            'created_at' => $this->created_at,
            'simple_created_at' => (string)$this->created_at,

            'updated_at' => $this->updated_at,
            'simple_updated_at' => (string)$this->updated_at
        ];
    }
}
