<?php

use App\Http\Controllers\Api\User\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix' => 'user',
        'namespace' => 'User',
        'as' => 'user.',
    ],
    function () {
        Route::controller(AuthController::class)->group(function () {
            Route::post('login', 'login')->name('login');
        });

        Route::group(
            ['middleware' => 'auth:users'],
            function () {
                Route::controller(AuthController::class)
                    ->group(
                        function () {
                            Route::post('logout', 'logout')->name('logout');

                            Route::get('profile', 'profile')->name('profile');
                        }
                    );
            }
        );
    }
);
