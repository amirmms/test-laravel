<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('mobile');
            $table->string('national_id')->unique();
            $table->string('password');
            $table->timestamps();
        });

        $user = User::create([
            'name' => 'امیر',
            'mobile' => '09364207924',
            'national_id' => '1271890372',
            'password' => '123456'
        ]);
    }
};
